# rhel7_kafka Minishift

## Rundown

This is a basic example of kafka running on Minishift; more specifically, 1 kafka broker pod, 1 zookeeper pod, 1 producer and 2 consumer pods for a total of 5 pods. 

The producer randomly generates (every 2-4 seconds) a topic, key, and value. The topic is either "fruit" or "vegetable", of which the key will respectively be a fruit or vegetable, and the value is a random integer. 

The consumers are based on the "fruit" or "vegetable" topics. They each only have one partition.

The broker and zookeeper are stateful deployments, and each have their own Persistent Volume Claims.

The minishift services connecting zookeeper and kafka are headless.

Zookeeper log data is stored at /var/lib/zookeeper/data on its PVC (although I'm pretty sure I've disabled saved logging).

Kafka topic data is stored at /var/lib/kafka/log-data on its PVC.

These settings are in the related config files in /rhel7_kafka/kafka-configs/config/.

## To deploy this...

### 1. Make a minishift project

Name this project rhel7-kafka

### 2. Build the Images

There are two RHEL7 images: "core" and "prodcon". 

"Core" is java-based and for what I call the 'Kafka Core'; it has Kafka (and the Zookeeper instance built into it) included in it.

"Prodcon" is Python 3.5-based and is for the producers and consumers. The producers and consumers are based on the kafka-python package. The Python scripts and package(s) themselves are NOT included and are thus included in an additional augmented s2i build on top of the included Dockerfile build.

The final image repo name scheme will be either "kafka" or "deploy-kafka", where plain "kafka" are just base images, and "deploy-kafka" are the deployment instances.

#### Image 1 (kafka-deploy:core):
a. Download itpaas-blessed-images/rhel7-platops-openjdk8:latest, tag it as rhel7-java:latest

b. from rhel7_kafka, build the Dockerfile located at /rhel7_kafka/dockerfiles/rhel-kafka_core/Dockerfile

c. tag this as deploy-kafka:core

d. push the built image to minishift

#### Image 2 (kafka-deploy:prodcon):
a. Run and build Jason's RHEL7 Python 3.5 image.

b. Tag this as kafka:prodcon

c. push the built image to minishift

d. run: oc apply -f /rhel7_kafka/openshift-yamls/prodcon_img/prodcon_buildconfig.yaml

e. run the build in minishift

### 3. Deploy the pods
a. Deploy Zookeeper: oc apply -f /rhel7_kafka/openshift-yamls/core_run/zoo.yaml

b. Deploy Kafka Broker: oc apply -f /rhel7_kafka/openshift-yamls/core_run/broker.yaml

c. Deploy the Producer: oc apply -f /rhel7_kafka/openshift-yamls/prodcon_run/producer_food.yaml

d. Deploy a Consumer: oc apply -f /rhel7_kafka/openshift-yamls/prodcon_run/consumer_fruit.yaml

e. Deploy a Consumer: oc apply -f rhel7_kafka/openshift-yamls/prodcon_run/consumer_vegetable.yaml