from time import sleep
from kafka import KafkaConsumer
import json

server="broker:9092"

def consume():
    sleep(5)
    consumer = KafkaConsumer(bootstrap_servers=server,
                             api_version=(0, 10, 1),
                             group_id="vegetable_consumers",
                             auto_offset_reset='earliest',
                             consumer_timeout_ms=30000,
                             session_timeout_ms=30000,
                             heartbeat_interval_ms=5000,
                             value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                             enable_auto_commit=False)
    print("Subscribing and initializing")
    consumer.subscribe(['vegetable'])
    consumer.poll(0)
    sleep(5)
    while True:
        msgs = consumer.poll()
        if msgs:
            msgs = list(msgs.values())[0]
            for msg in msgs:
                print("Offset:", msg.offset, "  Message:", msg.value)
        consumer.commit_async()
        sleep(5)


if __name__ == '__main__':
    consume()
