from time import sleep
from random import randint, choice
import requests

topics = ['fruit', 'vegetable']

items = {'fruit': ['apple', 'pear', 'grape', 'cherry'],
         'vegetable': ['carrot', 'lettuce', 'celery', 'pepper']}


def gen_random_message():
    sleep(15)
    while True:
        qty = str(randint(1, 5))
        topic = choice(topics)
        item = choice(items[topic])
        payload = {topic: {item: qty}}
        try:
            x = requests.post('http://producer:12345/', json=payload)
            print(x.status_code)
        except:
            print("oops! Message didn't work!")
        sleep(randint(1, 4))


if __name__ == "__main__":
    gen_random_message()
