from time import sleep
from random import randint, choice
from kafka import KafkaProducer
import json

topics = ['fruit', 'vegetable']

items = {'fruit': ['apple', 'pear', 'grape', 'cherry'],
         'vegetable': ['carrot', 'lettuce', 'celery', 'pepper']}


server="broker:9092"
# server="172.17.0.4:9092" #minishift

def gen_random_message():
    sleep(2)
    producer = KafkaProducer(bootstrap_servers=server,
                             api_version=(0, 10, 1),
                             value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    while True:
        qty = str(randint(1, 5))
        topic = choice(topics)
        item = choice(items[topic])
        print("sending:", topic, item, qty)
        producer.send(topic, {item: qty})
        sleep(randint(1, 4))


if __name__ == "__main__":
    gen_random_message()
